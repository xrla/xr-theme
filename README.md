# XR Grav Theme
This theme is largely mirrors the international site [rebellion.earth](https://rebellion.earth), and built specifically for XR Los Angeles. The theme is built for the [Grav](https://getgrav.org/) flat-file CMS system.  Please feel free to copy, improve upon and contribute. 

## Requirements
You can run it on any Apache/PHP or NGINX/PHP-FPM server running the underlying requirements found in Grav's [documentation](https://learn.getgrav.org/16/basics/requirements).  

## Installation
### Normal 
Download the compressed [Grav core](https://getgrav.org/downloads) and unzip into your web directory.  
Clone this repository (xr-theme) in place of the `grav/user` directory found in the Web server root directory.

### Docker 
Follow the directions found [here](sites/site-docs/pages/03.advanced/01.local-development/docs.md).  

## Documentation
* [Grav Documentation](https://learn.getgrav.org/16)  
* [Base Theme Documentation](https://learn.hibbittsdesign.org/openpublishingspace)  
* [XR Theme Documentation](sites/site-docs/pages)  *Renders to /site-docs if you run this theme in multisite subdirectory mode.*  

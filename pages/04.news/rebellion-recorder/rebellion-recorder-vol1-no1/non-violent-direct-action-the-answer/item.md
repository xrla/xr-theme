---
title: Non-violent Direct Action The Answer
subtitle: Writer Of First Book To Address Global Warming Believes NVDA To Be Earth’s Best Hope
author: Bill McKibben
date: 00:00 04/15/2019
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
    category: Rebellion Recorder
    author: Bill McKibben
    tag:
        - NVDA
---

*Bill McKibben —Founder 350.org*  

In the spirit of the Declaration of Independence, which began with an acknowledgement “that a decent respect to the opinions of mankind requires that we should declare the causes” of our action, it’s worth establishing for a watching world the reasons for this rebellion—or at least answering the questions that should spring quickly to mind when a rebellion is declared.

## Are we actually in an emergency?
This one’s easy. Human beings have never found themselves in an emergency of this scale. Indeed, you have to back many tens of millions of years to find an extinction crisis of the same scale. We know of five other periods like this over the last 400 million years, and all of them are connected with the same gas, carbon dioxide, that lies at the center of our current woes.

Since the advent of the Industrial Revolution, we have increased its concentration in the atmosphere by 50%, and as we burn more fossil fuel each year that concentration continues to increase. Simply put, physics makes the process and result clear: the molecular structure of Co2 traps heat in the atmosphere that would otherwise radiate out to space, consequently our earth has begun to rapidly warm.

The effects of that warming are now easily observable everywhere on the globe: All that was frozen has begun to melt. Warm air holds more water vapor than cold air, causing increased evaporation in arid areas (inviting drought and wildfire), and increasing storm activity and rainfall in wet areas (hence the record floods overwhelming us this year, again).

Every estimate is that the continued combustion of coal, gas, and oil will claim tens of millions of lives in the years to come, create hundreds of millions of refugees, and wipe entire nations off the map as oceans rise. Those hit first and hardest will the poorest and most vulnerable; the iron law of global warming is that those who did the least to cause it pay the highest price.

## Have we tried other means to slow it down?
We have. I can speak to this with some authority, having written the first book about climate change for a general audience way back in 1989. I have testified before Congress and state legislatures, and I have watched hundreds of others do the same. We have petitioned our leaders at every turn, and filed lawsuits seeking not money, but changes in behavior. Scientists have collaborated on massive reports, and scientific bodies have urged political leaders to act. We have mounted electoral campaigns, urged financial leaders to withdraw investments from reckless companies. We have built bike paths and put solar panels on our roofs. We have come together in vast marches. Our churches and faith communities have issued powerful, moral calls to action.

And the resulting action has been pitifully insufficient. The high water mark of global action was the Paris Climate Accords reached in 2015. But even this diplomatic pact, were every nation to meet its pledges, would have seen temperatures rise an impossible 3 degrees Celsius. And, of course, every nation has not met its pledges — our own has withdrawn from the Accords despite the fact that we have produced more greenhouse gases than any other nation on earth.

The reason for this woeful record is clear: the richest industry in the history of the planet, the fossil fuel industry, has used its wealth and power to systematically block action. Though we now possess, in the sun and wind, the technologies we need to reduce our impacts, our progress in that direction has been stalled and slowed by a perverse campaign of disinformation. Reporters have demonstrated beyond any doubt that oil companies knew about climate change in the 1980s, and began making their own plans in accord with that knowledge; Exxon, for instance, built its drilling rigs to compensate for the rise in sea level they knew was coming. But instead of telling the rest of us these companies spent billions of dollars to build an architecture of deceit, denial, and misinformation that has kept humankind from reacting with the vigor climate science demands. This treachery continues to this day.

## Is a non-violent response capable of changing this sad pattern?
It is the only thing that is.

Non-violence, a new ‘technology’ developed by people like Gandhi and Martin Luther King on the margins of power in the twentieth century, offers enormous hope. Examples around the world show that non-violent reistance can have a deep effect: subverting empire, upending segregation, challenging homophobia. It is not magic. Despite their great courage its practitioners have lost too, and we may indeed lose the climate fight since we have waited so very long to get started.

But no other force can rouse human conscience as quickly or change hearts and minds as deeply. Nothing else complicates life so thoroughly for the powerful. Already in the climate battle we’ve seen its effect. The first major climate deployment of civil disobedience came near the start of the battle over the Keystone Pipeline (KXL), when citizens from across the nation joined the indigenous people who had begun the fight. Mass arrests in Washington, on a scale not theretofore seen in the climate fight, spurred opposition not only to KXL but also, once it became clear the industry was not invulnerable, to every pipeline and LNG terminal, every new coal mine and fracking well. Some of these fights we win and some we lose, but taken together they have become a real problem for the industry.

Strict non-violence is, of course, essential. Anything else is a gift to the powers that be. And strict non-violence is sometimes hard. Going to jail is no fun. But it’s not, at least for people with a certain amount of privilege, the end of the world. The end of the world is the end of the world.

This is why we all must act.

*— Bill McKibben is an author, activist, educator, and founder of [350.org](https://350.org/). billmckibben.com*  
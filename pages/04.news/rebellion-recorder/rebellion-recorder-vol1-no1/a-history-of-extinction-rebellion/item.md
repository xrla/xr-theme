---
title: Who The Hell?! A History Of Extinction Rebellion
subtitle: Grassroots Group Seems To Come From Nowhere
author: Giselle Gonzales
date: 00:00 04/15/2019
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
    category: Rebellion Recorder
    author: Giselle Gonzales
    tag: History
# hero_image: cnn-offices.jpg
# body_classes: overlay-light
# hero_caption: "CNN officials stop photography of an action by SoCal350, Climate Strike LA, and XRLA at their Los Angeles offices."
# header_image_credit: J. Matt
---

*Giselle Gonzales —Founding Member, XRLA*  

On December 17 of last year, the University of Bristol psychology professor Colin Davis was arrested for spray painting hourglass iconography across government property in the middle of the day.

===

“What should we do when our government is leading us off the cliff edge? I’ve tried the conventional things. I’ve voted in elections, signed petitions, and written letters to my MP. I’ve been on marches, and given money to organizations...” he says, explaining his December actions.

Davis continued, “I’m not really sure that any of these things has had any positive effect, but I am sure that twelve more years of doing the same won’t be enough. By then we may have reached the point of runaway climate change. There isn’t time to wait for politicians to come to their senses. That’s why I’ve joined the Extinction Rebellion (XR) campaign.” Davis is part of a massive international campaign called Extinction Rebellion, a direct-action socio-political group focusing on the current existential climate crisis and impending ecological collapse.

![CNN Protest](cnn-offices.jpg)
<span class="image-caption">CNN officials stop photography of an action by SoCal350, Climate Strike LA, and XRLA at their Los Angeles offices. - <em>J. Matt, photo</em></span>  

Three and a half years earlier in the summer of 2015, Rising Up! formed in the United Kingdom. The network formed as an evolution of British organizers’ past coalitions in groups like Earth First!, Reclaim the Power, and Occupy.

Their aim was to reshape a broken system of entrenched political influence and power grown unaccountable to the greater public to achieve “a fundamental change of the [existing] political and economic system to one which maximizes well being and minimizes harm.” Out of this movement and history, Extinction Rebellion was born. XR’s battle to transform the system serving very few at the expense of the planet and its inhabitants is just getting started!

In late October 2018 XR UK made its initial splash when they released a public letter signed by 96 respected scientists and academics pleading with fellow citizens to become active in XR on behalf of all life on earth. They wrote:

*“The science is clear, the facts are incontrovertible, and it is unconscionable to us that our children and grandchildren should have to bear the terrifying brunt of an unprecedented disaster of our own making. We are in the midst of the sixth mass extinction, with about 200 species becoming extinct each day. Humans cannot continue to violate the fundamental laws of nature or of science with impunity. If we continue on our current path, the future for our species is bleak.”*

At the end of 2018 XRUK organized multiple successful and massively attended direct actions to bring awareness to the existential plight of all living things and to shift the Overton Window (the range of ideas tolerated in public discourse which shapes political viability of individual politicians and parties rather than their own preferred positions, named for political scientist Joseph P. Overton) and to aid in a more radical discussion of climate issues.

Their actions were bright, expressive, dire and yet still hopeful. Notably, XR UK called upon the need for action while simultaneously acknowledging the grief an understanding of our climate circumstance creates.

The worldwide impact of XR UK’s actions in London was stunning; spontaneously Extinction Rebellion cells began forming where ever people heard XR UK’s message. As of this writing there are 334 Extinction Rebellion groups on five continents. Testament to the universality of worldwide concern about global warming, XR is springing up in surprising places — the most recent nascent group is forming in Iran.

Here in Los Angeles, the first XR meeting was held in late 2018 with five founding members present. XRLA founding members had few plans or concrete ideas, but knew that they wanted to support the message of autonomy and action espoused by XR UK and radicalize Angelenos’ sentiments and activity regarding the climate crisis.

XRLA has continued growing. The cell participated in the official U.S. launch of Extinction Rebellion on January 26, 2019. XRLA held a rally on the 26th in Pershing square featuring six speakers from diverse walks of life speaking out about the ways global warming affects their lives and life on earth. More than 150 people were in attendance. Following the rally a march trekked across the city, flyering, swarming, and blocking traffic intermittently with banners declaring “CLIMATE EMERGENCY,” all while distributing informational flyers.
While all of this was happening downtown, XRLA simultaneously staged two banner drops over the 110 and 101 freeways that said “CLIMATE CHANGE = MASS MURDER.” XRLA ended the day with a funeral procession through Grand Central Market in Downtown Los Angeles bearing aloft a coffin labeled “OUR FUTURE.”

Since the actions on the 26th XRLA has attended or hosted events with 350.org, Ende Gelände, Sunrise LA, and the Students’ Climate Strike. Members are working furiously for Extinction Rebellion’s International April Week of Action from April 15-22. Get in touch to celebrate Earth Day on the 22nd with Extinction Rebellion LA.

See you there!

*— Giselle Gonzales is a cinematographer and one of XRLA’s founding members.*

![Eviair](eviair-carry-me-sally.png)
---
title: PG&E Acknowledges Camp Fire Start Responsibility
subtitle: State’s Largest Utility Indicates Utility Equipment Failure Likely Cause Of Deadly Fire That Killed 86
author: Staff Writers
date: 00:00 04/15/2019
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
    category: Rebellion Recorder
    tag:
        - Utility
        - Fire
---

*Staff Writers, San Francisco*  

On February 28th of this year PG&E conceded that its equipment was the likely cause of the Camp Fire in Butte County. The fire began on November 8th last year and burned 153,336 acres, destroyed 13,972 residences, 528 commercial buildings, and 4,293 other buildings. Eighty-six residents were killed in the fire, three remain missing. It was the worst wildfire to date in California. At fault was a transmission tower that by PG&E’s own accounting was 25 years past the end of its useful service life.  

The Camp Fire largely destroyed the towns of Paradise and Magalia as well as the unincorporated community of Concow. The fire also did substantial damage to the communities of Centerville, Butte Creek Canyon, and Yankee Hill and by its impacts to each of these locales created a domestic refugee crisis. More than 50,000 people have been displaced by the fire, cast off to face California’s already acute housing crisis.  

Installed during the administration of Woodrow Wilson (1913-1921) Tower 27/222 lost a wire in early morning heavy winds. Although the consequences of the service loss were negligible due to the failed transmission wire, within minutes of failure it started a brush fire that, by the time it was assessed by local fire authorities at 6:43 a.m., was being whipped by 35 mph winds and inaccessible due to the rugged terrain beneath the transmission lines.  

![Camp Fire](camp-fire.jpg)
<span class="image-caption">California foothill pine and bay laurel trees in the Camp Fire burn zone. 153,336 acres burned in the mega-fire. - <em>J. Matt, photo</em></span>

The first firefighter on the scene called out on the radio for an additional 15 engines, four bulldozers, two water tenders, four strike teams and hand crews. “This has got the potential for a major incident,” were his fateful words.  

PG&E equipment has been the cause of five of California’s most destructive wildfires since 2015. This in a landscape where six of California’s ten most destructive wildfires have burned in the last three years. With a long history of choosing to provide for shareholder profit over legally-mandated maintenance and safety preparations PG&E has been the shareholder owned utility in California with the deadliest track record of failures leading to fires and gas transmission or transformer explosions.  

Many of these failures have been fatal. The 2010 explosion of a thirty inch gas pipeline beneath a residential San Bruno neighborhood killed eight and leveled 35 homes, damaging many more. Subsequent investigation found negligent failure on the part of PG&E in storing, updating, and disseminating pipeline maps it is legally obligated to maintain and update. This led to the assumption that the failed line was sound when in fact it was built from a batch of pipe with a known flaw and required to have been repaired or replaced to prevent exactly the sort of accident that occurred. A Federal Grand Jury indicted PG&E for violations relative to the 1968 Pipeline Safety Act and on counts of obstruction of justice for lying to NTSB investigators in attempt to cover up its failures.  

Not all of PG&E’s equipment failures are deadly, some are merely spectacularly terrifying and maiming. In 2005 a woman walking on Post St. in San Francisco’s Financial District was admitted to the hospital in critical condition with burns on her face and neck. The explosion of an underground transformer buckled the sidewalk, sent a manhole cover through a shop window and sent flames onto the sidewalk, burning her. SFPD evacuated hundreds in the area believing the explosion to have been a bombing.  

![Mama Celeste’s Pizzeria](mama-celeste-pizzeria.jpg)
<span class="image-caption">Mama Celeste’s Pizzeria, Paradise. Celeste’s and 46 other businesses were destroyed in the Skyway business district. - <em>J. Matt, photo</em></span>

In 2011 underground explosions in downtown San Francisco blew two 100lb manhole covers into the air, fortunately injuring no one. Between 2005 and 2016 in the Bay Area there were 78 underground explosions or fires involving PG&E equipment. San Francisco loses manhole covers with stunning regularity, often shattered by the force of underground blasts caused by failing PG&E equipment.
PG&E, in response to the liabilities incurred by its wildfire starts, declared bankruptcy January 29th of this year in an attempt to protect the utility from direct liabilities and from paying damages to those of its 16 million customers who have been killed, injured, or otherwise affected by the utility’s myriad equipment failures and the disasters that have followed.  

This is not the first bankruptcy for the utility. In 2001 PG&E declared bankruptcy when, like SoCal Edison, it found itself unable to buy electricity on the wholesale market due to ruined credit caused in part by wholesale market manipulation by Enron and other unethical wholesalers raising prices beyond PUC contract billing allowances. 2001’s bankruptcy came after surprising utility market watchers and besting 1999 operating earnings in 2000, allowing PG&E to pay out higher dividends than expected and raising share value.  

In negotiations prior to declaring insolvency PG&E’s holding company was unwilling to sell transmission infrastructure to California in exchange for the state’s stepping in and buying power on behalf of PG&E, as SoCal Edison did to avoid its own bankruptcy. At the time there was a sense among ratepayers that PG&E was being squeezed for cash by its publicly traded holding company for shareholder benefit and that ratepayers would be left holding the bag. The payment of $17.5 million in bonuses to management and executive level employees while under bankruptcy supervision didn’t help perceptions of venality. Today PG&E ratepayers pay nearly double the national average for power.  

The moving of cash out of the utility to the holding company became an issue for PG&E again in 2010's San Bruno pipeline explosion. A state investigation precipitated by the explosion determined that over a fifteen year period PG&E had diverted $100 million in gas pipeline safety and operations money collected from ratepayers to other purposes; principally stockholder payments and executive bonuses. Included in this, according to the report, was “a significant portion, in the millions, [that] has been awarded to the CEO,” former PG&E head Peter Darbwee.  

In early March this year US District Judge William Alsup, overseeing PG&E’s probation from the San Bruno explosion, excoriated PG&E in the wake of the Camp Fire saying, “The essence of the problem is that the offender’s unsafe conduct led to a deadly pipeline explosion and to six felony convictions. Now, the offender’s unsafe conduct has led to recurring deadly wildfires caused by its electrical system.” In 2016 and 2017 PG&E paid dividend payments to shareholders of $1.92 billion. Alsup ordered an end to these payments until the utility was in compliance with its vegetation management obligations.  

The city of San Jose has become the first city in PG&E’s service area to break from its power provision, starting its own publicly owned community choice power aggregator supplying renewable power city-wide. Its service began this March and is relying on PG&E for its grid and billing services. Might San Jose soon be able to buy the electrical infrastructure it needs, cheap, at a bankruptcy sale? Perhaps PG&E’s failures and San Jose’s lead can be an opportunity for public power in the service area of the nation’s largest utility and beyond. California is being shown it has a choice — to continue with dangerously managed for-profit utilities or take the reins itself and deliver carbon-free power statewide.

----
## What you can do:
* [North Valley Community Foundation](http://tinyurl.com/y9fcvx7h) is distributing funds to qualifying organizations working in the aftermath of the Camp Fire.  
* The highly regarded [California Community Foundation Fund](http://tinyurl.com/y8vm2nd4) is distributing funds for Camp, Woolsey, and Hill fire victims providing mental health and rebuilding/ resettling assistance.

*Our hearts go out to all who have been affected by California’s fires.*
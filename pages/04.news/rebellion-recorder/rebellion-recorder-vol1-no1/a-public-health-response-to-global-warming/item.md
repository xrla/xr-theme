---
title: A Public Health Response To Global Warming
subtitle: Public Health Methods Change Outcomes
author: Alissa Nelson
date: 00:00 04/15/2019
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
    category: Rebellion Recorder
    author: Alissa Nelson
    tag:
        - Public Health
---

*Alissa Nelson —Public Health Consultant*  

Public health has done an extraordinary job of making humans capable of living in huge, urban environments. We have doubled, and in some places tripled the human life expectancy, allowing us to efficiently treat and even prevent the problems of communicable diseases of all stripes, the chronic diseases and cancers borne of aging and occupational exposures, even the seemingly intractable problem of pollution.  

We are also very good—by way of advances in data collection and computational modeling—at predicting what might happen as a result of global warming.  

## Here is some of what we know:

* Health impacts of global warming and the underlying causes of global climate change: According to The Lancet, 125 million more vulnerable elderly people were exposed to heatwaves in 2016 than in 2000.  
* Extreme heat exacerbates existing health conditions:  
    * Rates of death from heart failure and kidney disease rise due to stress from heat stroke.  
    * Some of these same health conditions are seen in higher rates with exposure to air pollution.  
* Due to changes in climate in countries where dengue fever is endemic, there is a 9.5% increase in the capacity of mosquitoes to transmit dengue – and ranges for the main mosquito that transmits dengue are shifting.  
* The people most likely to be affected are rural laborers like farm workers, people without access to sufficient clean water, and people in the global south in general.  

![Drill Rig](drill-rig.jpg)
<span class="image-caption">Long Beach well at Willow Creek Park adjacent to Sunnyside Cem- etary; oil extraction from park to grave. - <em>Mark Campbell, photo</em></span>
## Things we can do to reduce carbon in the atmosphere that also improve our health:

* Eat less meat. This preserves undeveloped land, is better for our water sources and oceans and helps to preserve fisheries. It also reduces the risk of a myriad of different cancers and chronic diseases.  
* Build sustainable local food systems. This improves health in rural and urban neighborhoods in many ways, particularly by reducing transportation and increasing
access to healthy, safe food.  
* Build cities that are centered around active transportation and public transportation. More exercise is always good for physical and mental health, less fossil fuel burning
means better air quality.  
* More access to public transportation increases access to economic opportunity, particularly in rural and low income neighborhoods.  

--- 

Amazingly, public health approaches that improve our health have an immediate and notable impact on our environment: walkable, bikeable cities with efficient public transit reduce the use of cars cutting emissions while improving population health and social connectivity — all while improving the economic vitality of communities.
Buying locally grown, organic produce benefits the health of farm workers and the economic vitality of rural communities while reducing our reliance on long-distance fossil fuel-intensive transportation of less nutritionally-dense food. These outcomes are widely acknowledged, and yet at every turn, efforts to improve the health of vulnerable rural, indigenous, immigrant, and black communities are stymied by agribusiness and fossil fuel companies seeking to perpetuate their power and wealth at the expense of human health.
It is our responsibility to act in the face of these realities. The eleven years we have to cut carbon output and effect a positive change on our climate future is not a long time to shift the global systems of health. We do not have the systems of care in place in much of our county to do this, let alone across the globe. As professionals with the avowed duty to care for others, public health workers must address these challenges in all we do, from how we treat our patients, to how we treat our workforces. Our patient is also the earth and its condition is dire.

*—Alissa Nelson, MPH/MSW, MA is a public health consultant whose clients include LAC DMH. [alissanelson.com](https://www.alissanelson.com/)*
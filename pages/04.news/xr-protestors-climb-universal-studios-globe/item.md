---
title: XR Protestors Climb Universal Studios Globe
date: 16:00 04/22/2019
published: true
show_breadcrumbs: true
taxonomy:
    category: Action
    type: Action
    tag: 
        - Universal Studios
        - News Coverage
---

<blockquote>Two people scaled the iconic globe outside Universal Studios on Monday as part of an Earth Day demonstration. The members of the Los Angeles chapter of Extinction Rebellion — a group of climate change activists — waved green flags that read “climate emergency,” while they sat atop the structure, claiming to be superglued. - <cite>Colleen Shalby, LA Times</cite></blockquote>

===

![XR Universal Studios Protest](universal-studios-color.jpg)
<span class="image-caption"><em>J. Matt, photo</em></span>

## News Articles
[ABC7](https://abc7.com/society/demonstrators-climb-universal-studios-globe-demand-climate-change-action/5265458/)  
[CBS LA](https://losangeles.cbslocal.com/2019/04/22/protesters-globe-universal-studios-earth-day/)  
[Deadline](https://deadline.com/2019/04/earth-day-protesters-scale-universal-studios-globe-1202600116/)  
[Environmental News Service](https://ens-newswire.com/2019/04/22/earth-day-2019-marked-by-climate-concerns/)  
[Evening Standard](https://www.standard.co.uk/news/world/ecowarriors-glue-themselves-to-universal-studios-globe-in-hollywood-a4123781.html)  
[Fox LA 11](https://www.foxla.com/news/local-news/climate-activists-climb-superglue-themselves-to-globe-at-universal-studios)  
[Fox News](https://www.foxnews.com/us/activists-universal-studios-globe-california-climate-change)  
[Hollywood Reporter](https://www.hollywoodreporter.com/news/climate-change-protesters-climb-universal-studios-globe-1203824)  
[Indybay](https://www.indybay.org/newsitems/2019/04/23/18822917.php#18822924)  
[KTLA 5](https://ktla.com/2019/04/22/2-protesters-scale-globe-at-universal-studios-in-earth-day-demonstration/)  
[LA Magazine](https://www.lamag.com/citythinkblog/extinction-rebellion-protest/)  
[LA Times](https://www.latimes.com/local/lanow/la-me-earth-day-protest-universal-studios-20190422-story.html)  
[NBC LA](https://www.nbclosangeles.com/news/local/Universal-Studios-Globe-Protest-Climb-Earth-Day-508912871.html)  
[NY Daily News](https://www.nydailynews.com/news/national/ny-universal-globe-activists-superglue-climate-change-nbc-20190422-crfjlgkfrzblhkfe6w7yk7uglq-story.html)  
[Teen Vogue](https://www.teenvogue.com/story/extinction-rebellion-climate-activists-glued-to-universal-studios-globe-los-angeles)  
[The Week](https://theweek.com/10things/827148/10-things-need-know-today-april-23-2019#item-3)  
[Theme Park Insider](https://www.themeparkinsider.com/flume/201904/6739/)  
[Washington Post](https://www.washingtonpost.com/nation/2019/04/23/hundreds-arrested-extinction-rebellion-protests-against-climate-change/)  



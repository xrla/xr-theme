---
title: Resources
hide_git_sync_repo_link: false
sitemap:
    changefreq: monthly
blog_url: /resources
show_sidebar: false
show_breadcrumbs: false
show_pagination: true
content:
    items: '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
submenu:
    flyer: 
        title: Flyers
        category: Flyer
    poster: 
        title: Posters
        category: Poster
    ideology: 
        title: Ideology
        category: Ideology
bricklayer_layout: true
display_post_summary:
    enabled: false
feed:
    description: 'XRLA Resources'
    limit: 10
pagination: true
---

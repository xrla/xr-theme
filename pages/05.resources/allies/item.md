---
title: Allies
subtitle: Who are we inviting to the dance? All of you!
date: 00:00 05/19/2019
taxonomy:
    category: Ideology
    type: PDF
    tag: 
        - Solidarity
        - Intersectionality
        - Allies
show_breadcrumbs: true
---

When examined, global warming seems to be a huge problem which should obviously cut across all interest groups, political divides, and existing social movements. No matter what issue moves us, we all, like it or not, have to live here on earth! We don't bring up this reality to devalue other issues we face or the social movements that confront them, but to point out how foundational an issue global warming and environmental degradation is to life on earth. Saving our planet from what the vast majority of scientists tell us is an existential, human-caused crisis seems, at the surface, to be the one thing that every person, young or old, living on earth can get behind. Right?  

And for the most part global warming is appearing to be on its way to becoming that — an issue that moves people across a myriad of divides that previously might have separated them. Certainly young people today have more stake in turning the atmospheric carbon tide and reining in species and habitat loss than people whose time on earth might be limited due to their age. This idea seems to be both common sense, and bearing out to be the truth of the matter and in many ways the kids are leading the way. The international youth movements becoming active relative to these issues can be understood as a direct manifestation of the future speaking to today's status quo. As local youth groups such as Youth Climate Strike L.A. and the Los Angeles chapter of the Sunrise organization work to fight against the earth's becoming a miserable, uninhabitable place they can be seen as natural Extinction Rebellion allies. The kids are the future! (Again.)  

But who else might be an ally or potentially swell Extinction Rebellion Los Angele's (XRLA's) activist membership ranks to help these kids? Quite simply, *anyone*. Anyone that can abide by XRLA's tenets of non-violence as described in our Ideology, Goals, and Principals statement is an ally and potential member. Our climate reality demands that this movement not be limited to self-identified activists but be inclusive of all people who are concerned about preserving a viable future for life on earth, activist or not. These people can come to XRLA by way of already organized groups working at the front lines of local environmental activism (from small local groups concerned with specific issues such as neighborhood oil drilling to local chapters of national organizations with larger mandates). They can come to XRLA by way of becoming individually awakened to the gravity of the earth's situation relative to the foundation-shattering impacts of global warming and species and habitat loss. They can come to XRLA by way of school, church, or social groups. They can come to XRLA by way of... you're getting the picture. If you see the moral imperative of the work we do, WE WANT YOU! Hurry though, we've only about ten years left to tackle this mess....  

Today's climate crisis demands that any Angeleno be considered a potential ally. Any Angeleno who sees that the status quo is not working; that our federal, state, and local governments aren't addressing the issues at hand as the emergencies they are; that our future on earth *quite literally depends* on making sustainable changes to the way we have chosen to live on the planet which sustains us, is welcome to join us to help secure a livable future on earth! We look forward to your help... the kids are going to need it. We are all going to need it.

<a href="/user/pages/05.resources/allies/allies.pdf">Allies PDF <i class="fas fa-file-pdf"></i></a>  

[pdfjs file=allies.pdf height=900]

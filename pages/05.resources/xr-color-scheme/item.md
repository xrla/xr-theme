---
title: "Extinction Rebellion Color Scheme"
date: '00:00 01/01/2019'
taxonomy:
    category: Design
    tag:
        - Design
        - Website
show_breadcrumbs: true
---

## Main Colors
When you see the logo it’s often black on green, but it can also sit on the other backgrounds.

### Green
<div class="bg-green color-block"></div>
**RGB:** 20, 170, 55  
**CMYK:** 88%, 0%, 68%, 33%  
**HSV:** 134°, 88%, 67%  
**HSL:** 134°, 78%, 38%  
**HEX:** #15ac39  
**PMS:** 375  
<div class="clear"></div>

### Black
<div class="bg-black color-block"></div>
**RGB:** 0, 0, 0  
**CMYK:** 0%, 0%, 0%, 100%  
**HSV:** 0°, 0%, 0%  
**HSL:** 0°, 0%, 0%  
**HEX:** #000000  
**PMS:** 6  
<div class="clear"></div>

## Bright Colors
Our colours symbolise the intersectional nature of XR. The colours can and should be mixed together to create bold and bright graphics.

### Lemon
<div class="bg-lemon color-block"></div>
**RGB:** 247, 238, 106  
**CMYK:** 0%, 4%, 57%, 3%  
**HSV:** 56°, 57%, 97%  
**HSL:** 56°, 90%, 69%  
**HEX:** #f7ee69  
**PMS:** 602  
<div class="clear"></div>

### Light Blue
<div class="bg-blue color-block"></div>
**RGB:** 117, 208, 241  
**CMYK:** 51%, 14%, 0%, 5%  
**HSV:** 197°, 51%, 95%  
**HSL:** 197°, 83%, 71%  
**HEX:** #78d0f2  
**PMS:** 297  
<div class="clear"></div>

### Pink
<div class="bg-pink color-block"></div>
**RGB:** 237, 155, 196  
**CMYK:** 0%, 35%, 17%, 7%  
**HSV:** 329°, 35%, 93%  
**HSL:** 329°, 70%, 77%  
**HEX:** #ed9bc6  
**PMS:** 236  
<div class="clear"></div>

### Purple
<div class="bg-purple color-block"></div>
**RGB:** 153, 98, 151  
**CMYK:** 0%, 36%, 1%, 40%  
**HSV:** 302°, 36%, 60%  
**HSL:** 302°, 22%, 49%  
**HEX:** #986197  
**PMS:** 2617  
<div class="clear"></div>

### Light green
<div class="bg-light-green color-block"></div>
**RGB:** 190, 210, 118  
**CMYK:** 10%, 0%, 44%, 18%  
**HSV:** 74°, 44%, 82%  
**HSL:** 74°, 50%, 64%  
**HEX:** #bcd175  
**PMS:** 387  
<div class="clear"></div>

### Warm yellow
<div class="bg-yellow color-block"></div>
**RGB:** 255, 193, 30  
**CMYK:** 0%, 24%, 88%, 0%  
**HSV:** 44°, 88%, 100%  
**HSL:** 44°, 100%, 56%  
**HEX:** #ffc31f  
**PMS:** 108  
<div class="clear"></div>

### Bright Pink
<div class="bg-bright-pink color-block"></div>
**RGB:** 207, 98, 151  
**HEX:** #cf6397  
**Not used offline**  
<div class="clear"></div>

### Red
<div class="bg-red color-block"></div>
**RGB:** 220, 79, 0  
**CMYK:** 0%, 64%, 100%, 14%  
**HSV:** 22°, 100%, 86%  
**HSL:** 22°, 100%, 43%  
**HEX:** #db5000  
**PMS:** 1665  
<div class="clear"></div>

### Dark blue
<div class="bg-dark-blue color-block"></div>
**RGB:** 56, 96, 170  
**CMYK:** 67%, 44%, 0%, 33%  
**HSV:** 219°, 67%, 67%  
**HSL:** 219°, 51%, 45%  
**HEX:** #3861ad  
**PMS:** 3005  
<div class="clear"></div>

### Angry
<div class="bg-angry color-block"></div>
**RGB:** 199, 0, 129  
**CMYK:** 0%, 100%, 35%, 22%  
**HSV:** 321°, 100%, 78%  
**HSL:** 321°, 100%, 39%  
**HEX:** #c70081  
**PMS:** 239  
<div class="clear"></div>

---
## PDF version

[pdfjs file=colrPALT.pdf height=900]
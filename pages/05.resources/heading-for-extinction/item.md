---
title: Heading For Extinction
subtitle: What To Do About It
date: 00:00 08/12/2019
taxonomy:
    category: Presentation
    author: Alissa Nelson
    type: Form
    tag: 
        - Call To Action
        - Slide Deck
        - Presentation
        - Talk
        - PDF
        - Powerpoint
show_breadcrumbs: true
---
Sign up to receive links to the presentation Heading for Extinction and what to do about it.

===

<link href='https://actionnetwork.org/css/style-embed-v3.css' rel='stylesheet' type='text/css' />
<script src='https://actionnetwork.org/widgets/v3/form/heading-for-extinction-and-what-to-do-about-it-download-the-talk-and-speakers-guide?format=js&source=widget'></script>
<div id='can-form-area-heading-for-extinction-and-what-to-do-about-it-download-the-talk-and-speakers-guide' style='width: 100%'><!-- this div is the target for our HTML insertion --></div>
---
title: Black Lives Matter
website: https://blacklivesmatter.com/chapter/blm-los-angeles/
facebook: blmla
twitter: BLMLA
instagram: blmlosangeles
logo: logo.png
---

**Black Lives Matter** is an international activist movement, originating in the African-American community, that campaigns against violence and systemic racism towards black people.
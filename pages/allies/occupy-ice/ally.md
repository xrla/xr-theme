---
title: Occupy ICE
website: https://occupyice.org/
facebook: occupyice
twitter: occupyice
logo: logo.jpg
---

**Occupy ICE** is a series of protests, modeled on the Occupy Movement, that emerged in the United States in reaction to the Trump administration family separation policy, with a goal of disrupting operations at several U.S. Immigration and Customs Enforcement (ICE) locations. 
---
title: March & Rally LA
website: https://www.marchandrallyla.com/
facebook: MarchAndRallyLA
twitter: marchnrallyla
instagram: marchandrallyla
logo: logo.jpg
---

Volunteers with **March & Rally LA** speak at city council meetings, show up for protests, phone bank and petition elected officials, connect communities across issues, and provide creative issue advocacy so that our combined numbers make real change happen. Connect with us, and become an activist.
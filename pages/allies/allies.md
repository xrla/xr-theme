---
title: Allies
content:
    items: '@self.children'
    limit: 0 
classes: bg-purple
---

When examined, global warming seems to be a huge problem which should obviously cut across all interest groups, political divides, and existing social movements. No matter what issue moves us, we all, like it or not, have to live here on earth! We don't bring up this reality to devalue other issues we face or the social movements that confront them, but to point out how foundational an issue global warming and environmental degradation is to life on earth. Saving our planet from what the vast majority of scientists tell us is an existential, human-caused crisis seems, at the surface, to be the one thing that every person, young or old, living on earth can get behind. Right? 
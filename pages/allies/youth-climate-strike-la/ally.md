---
title: Youth Climate Strike LA
website: https://www.youthclimatestrikeus.org/california
facebook: California-Climate-Strike-321523718719958
twitter: climatestrikeca
instagram: climatestrikeca
logo: logo.jpg
---

**Youth Climate Stike**, the youth of America, are striking because the science says we have just a few years to transform our energy system, reduce our greenhouse gas emissions, and prevent the worst effects of climate change.
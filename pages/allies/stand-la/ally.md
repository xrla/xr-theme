---
title: Stand LA
website: https://www.stand.la/
facebook: STANDLosAngeles
twitter: STAND_LA
instagram: stand_losangeles
logo: logo.jpg
---

**STAND-L.A.** is an environmental justice coalition of community groups that seeks to end neighborhood drilling to protect the health and safety of Angelenos on the front lines of urban oil extraction.
---
title: East Yard Communities for Environmental Justice
website: http://eycej.org/
logo: logo.png
---

**East Yard Communities for Environmental Justice (EYCEJ)** is an environmental health and justice non-profit organization working towards a safe and healthy environment for communities that are disproportionately suffering the negative impacts of industrial pollution. Through grass-roots organizing and leadership building skills, **EYCEJ** works to enable under-represented communities to be heard, which in turn influences policy change, policy makers and agencies that can institute health protective environmental justice policies that are in the best interest of local, regional, and statewide residents.
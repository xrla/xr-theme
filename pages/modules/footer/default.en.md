---
title: Footer
routable: false
visible: false
left: 
    -
        title: The Rebellion
        links: 
            - 
                name: About
                url: /rebellion#about
            -
                name: Demands
                url: /rebellion#demands
            - 
                name: Principles
                url: /rebellion#principles
    - 
        title: The Extinction
        links:
            - 
                name: The Truth
                url: /extinction#the_truth
            - 
                name: The Extinction
                url: /extinction#emergency
middle:
    -
        title: Other
        links: 
            - 
                name: Allies
                url: /allies
            - 
                name: News
                url: /news
            -
                name: Contact
                url: /#take_action
---
---
title: Privacy Policy & Terms of Use
routable: false
visible: false
--- 

## Who we are

Our website address is: [https://xrla.org](https://xrla.org).  

## What personal data we collect and why we collect it  

### Sign up form

Our signup form connects to [https://actionnetwork.org/](https://actionnetwork.org/). Contact details from sign up form submissions are stored in our account in The Action Network.

### Cookies

We do not use Cookies to track our sites guests.

### Embedded content from other websites

Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.

These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.

### Analytics

We use Cloudflare analytics to calculate unique visitors which is done by your IP address.

## Who we share your data with
### How long we retain your data

For users that submitted the sign up form we retain the details provided in our ‘The Action Network’ account [https://actionnetwork.org/](https://actionnetwork.org/). We do not share your information outside of Extinction Rebellion US chapters.

### What rights you have over your data

You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.

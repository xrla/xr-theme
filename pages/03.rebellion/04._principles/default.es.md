---
title: 'Principios'
classes: 'text-left bg-blue'
---

# Principios{.red-line}
Al igual que el grupo internacional XR, tenemos diez principios básicos a partir de los cuales crecen nuestras acciones.  

**Son:**  
1. Todos nosotros en XRLA tenemos una visión compartida del cambio.  
1. La misión de XRLA es lo que se necesita: movilizar al 3.5% o más de la población del Condado de Los Ángeles para lograr un cambio sistémico en Los Ángeles al servicio de la parte de Los Ángeles en el mantenimiento de un aumento de 1.5&deg;C de la temperatura global promedio sobre los promedios preindustriales.  
1. XRLA cree que Los Ángeles y los Angelinos necesitan una cultura sostenible. Nuestra cultura debe ser saludable para todos, resistente y adaptable para enfrentar un futuro desestabilizado por las desastrosas decisiones de nuestra sociedad.  
1. XRLA desafía abiertamente a sus miembros a que se levanten para rehacer los sistemas tóxicos de gobierno y economía que hemos creado en los Estados Unidos. XRLA cree que *todos* en los Estados Unidos deben cuestionar su papel en perpetuar una ideología que amenaza la supervivencia de nuestro planeta, como recordamos en nuestro pasado reciente y conózcalo hoy, incluso, y *especialmente a los miembros de XRLA*.  
1. XRLA valora la reflexión y el aprendizaje de las acciones públicas de nuestro grupo y las de otros movimientos populares. Tomaremos las lecciones que aprendamos para planificar más acciones con cada acción pública, aprendiendo de los fracasos y éxitos de las acciones públicas anteriores y aprovechándolos de ellos.  
1. XRLA da la bienvenida a todos y cada parte de todos aquellos que deseen trabajar dentro de nuestro marco. Trabajamos para crear activamente espacios más seguros y accesibles para todos los que vienen con un propósito genuino para ayudarnos a alcanzar nuestras metas en Los Ángeles.  
1. XRLA busca activamente refundir las dinámicas de poder y las estructuras en Los Ángeles con el objetivo de rehacer las jerarquías de poder locales existentes para fomentar una participación más equitativa y justa de todos los angelinos.  
1. XRLA no culpará ni avergonzará. Creemos que nuestro sistema actual de gobierno y economía es tóxico y, en última instancia, mortal para la Tierra tal como la conocemos. Reconocemos que todos tenemos y continuamos desempeñando un papel en la creación y la perpetuación de este sistema. Nadie está libre de culpa y nadie puede trabajar para realizar los cambios necesarios para la supervivencia saludable de todos. Estamos juntos en esto, hemos creado esta crisis, debemos trabajar para evitar esta crisis.  
1. XRLA es una célula no violenta en una red no violenta que gana fuerza como parte de un todo.  Usaremos exclusivamente estrategias y tácticas no violentas para lograr nuestros objetivos y creemos que son la forma más efectiva de fomentar el cambio que requerimos.  
1. XRLA existe como una parte autónoma de un todo descentralizado y nuestro grupo individual practica la toma de decisiones colectiva y no tiene un liderazgo que pueda ser señalado como "a cargo" de cualquier decisión o acción en particular. Somos un colectivo que crea una estructura colectiva con la que trabajamos para desafiar un status quo mortal. Nosotros, como individuos en XRLA, actuamos de acuerdo con el principio moral objetivo de que explotar los recursos de nuestro planeta de una manera que cree la probabilidad probable de la destrucción de la vida en la tierra como lo entendemos hoy en día es categóricamente incorrecto y *debe ser detenido*.   

<div class="text-center"><img src="/user/themes/xrla/images/xr-hourglass-black.svg" width="20"/> **Actuamos en paz con amor feroz de estas tierras en nuestros corazones.** <img src="/user/themes/xrla/images/xr-hourglass-black.svg" width="20"/></div>

* Cualquier persona que sea capaz de seguir estos diez principios principales puede unirse a nuestra acción colectiva.  
* Nos negamos a legar un planeta moribundo a las generaciones futuras al no actuar ahora.  
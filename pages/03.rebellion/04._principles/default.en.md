---
title: 'Principles'
classes: 'text-left bg-blue'
---

# Principles{.red-line}
XR LA has ten basic principles from which our actions and structure grow, based on those of the international Extinction Rebellion movement:

**They are:**  
1. We have a shared vision of change, to create a world that is fit for generations to come.
1. We set our mission on what is necessary — mobilizing 3.5% or more of the population to achieve systemic change and end the climate emergency, using ideas such as momentum-driven organizing to achieve this.
1. We need a regenerative culture. We will model a community that is healthy, resilient, and adaptable to ecological change.
1. We openly challenge our members to rise up and challenge themselves and toxic systems. Everyone must question their role in the structures that threaten the survival of our planet.
1. We value reflecting and learning from our group's actions, following an ongoing cycle of action, reflection, learning, and planning for more action. We will take lessons from our elders and other popular movements in service of this goal.
1. We welcome everyone and every part of everyone. We work to actively create safer and more accessible spaces, open to any and all dedicated to our principles.
1. We actively seek to reimagine and recast power dynamics and structures, with the goal of breaking down hierarchies, to foster the equitable and just participation of all.
1. We avoid blaming and shaming. Our present system of governance and economics is toxic. We acknowledge that we all play a role in perpetuating this system, but no one individual or group is to blame.
1. We are a non-violent cell in a non-violent network and will exclusively use non-violent strategies and tactics as the most effective way to bring about change. 
1. We are an autonomous part of a decentralized whole. We collectively create the structures we need to challenge power.

<div class="text-center"><img src="/user/themes/xrla/images/xr-hourglass-black.svg" width="20"/> **Any who agree to these principles are welcome to participate in our movement.** <img src="/user/themes/xrla/images/xr-hourglass-black.svg" width="20"/></div>

* We refuse to bequeath a dying planet to future generations by failing to act now.  
* We act in peace with ferocious love of these lands in our hearts.  
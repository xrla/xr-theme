---
title: 'About'
---

# QUE DEMONIOS ES REBELIÓN DE EXTINCIÓN LOS ANGELES?{.red-line}

Extinction Rebellion Los Angeles (XRLA) es una celula del organismo internacional de Extinction Rebellion que hizo su debut en Londres en Octubre del 2018 después de estar incubandose durante casi dos anos de Rising UP!, una incubadora de activistas de acción directa sin violencia. Desde May Day (Dia del Trabajador Internacional) 2019 hay circa de 400 células en cinco continentes — Una diffusion increíble de movimiento social bajo cualquier medida.

![Extinction Rebellion Butterfly](xrla-hourglass-butterfly-logo.svg?resize=200,200){.float-left}
Extinction Rebellion Los Angeles nació en Enero de 2019 y desde nuestro mas reciente genesis hemos estado trabajando en apoyo activo de nuestro grupo local Youth Climate Strike, SunriseLA (dos organizaciones de activismo juvenil) y StandLA (un esfuerzo de largo plazo para poner fin a la extracción de petróleo del vecindario en L.A.), entre otros. Hemos publicado la primera edición de nuestro periódico, The Rebellion Recorder, una verdadera prensa en vivohoja ancha con exclusivas
de Bill McKibben y Peter Kalmus. Tomamos control del globo de Universal Studios con intention de exigir que Hollywood deje negocios despilfarrantes y contaminantes de carbono como de costumbre y que en su lugar utilice su influencia mundial para alertar a la gente de los peligros del calentamiento global como una crisis urgente. Esta acción fué vista a través de medios de comunicación en todo el mundo y se puede ver en nuestra pagina web [xrla.org](https://xrla.org).

XRLA tiene como meta los objetivos generales del movimiento global pero con foco en Los Angeles. Esto se puede detallar :

![Extinction Rebellion Hand Pointing](xr-hourglass-hand-pointing.svg?resize=200,200){.float-right}
Nosotros en XRLA tenemos objetivo de sacudir a 3.5% de Angelenos de sus vidas cotidianas para unirse activamente a nosotros en exigir acción realista y oportuna del calentamiento global por nuestros gobiernos para evitar los peores efectos del calentamiento global que la ciencia climática nos indica su pronta llegada. Haremos esto por medio de actos no violentos desobediencia civica, negociacion, educacion, el apoyo de y colaborando con los grupos locales existentes trabajando en las intersecciones de la justicia ecológica y climática, justicia social, y cualquier otro grupo local que observa nuestra realidad climática actual para lo que es — una amenaza existencial a la vida en la tierra como hemos llegado a conocerlo. Somos una llamada de atencion para L.A. con intencion de motivar a 142,000 personas hacia accion en la ciudad de L.A. solamente!

Nuestra situation climatic es grave y con solo diez anos efectivamente restantes para mantener +1.5oC de aumento medio sobre las temperaturas atmosféricas medias históricas preindustriales y el tiempo se nos agota. El desmembrado de la industria, gobierno, y grupos de apoyo que nos ha llevado a este lugar terrible no se mantendrá. Extinction Rebellion es un movimiento del pueblo y no sera cooptado. Nuestra continua supervivencia en la tierra exige que tengamos éxito, las consecuencias de fracaso no son aceptables. 

Unete a nosotross! Para un futuro; para tu futuro, el futuro de tus hijos, el futuro de la tierra en que vivimos. Te das a la idea...

<div class="text-center"><img src="/user/themes/xrla/images/sabertooth-skull.svg" height="80" /> <strong class="h2" style="vertical-align: top;top: 9px;position: relative;">NUESTRO FUTURO SE GANARA GRADO A GRADO</strong> <img src="/user/themes/xrla/images/human-skull.svg" height="80" /></div>



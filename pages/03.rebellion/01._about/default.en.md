---
title: "About"
---

# What Is Extinction Rebellion Los Angeles?{.red-line}

Extinction Rebellion is an international movement that uses non-violent civil disobedience to achieve radical change in order to minimize the risk of human extinction and ecological collapse. Extinction Rebellion Los Angeles (XRLA) is a local cell of this international movement which has over 400 independent cells on five continents.

![Extinction Rebellion Butterfly](xrla-hourglass-butterfly-logo.svg?resize=200,200){.float-left}
Our world is in crisis. Life itself is under threat. Yet every crisis contains the possibility of transformation. Across the world, heralded by the young, people are waking up and coming together.

We are a wake up call to L.A. and to the world! We are XR and you are us. Join us!

<div class="text-center"><img src="/user/themes/xrla/images/sabertooth-skull.svg" height="80" /> <strong class="h2" style="vertical-align: top;top: 9px;position: relative;">OUR FUTURE WILL BE WON BY DEGREES</strong> <img src="/user/themes/xrla/images/human-skull.svg" height="80" /></div>

<div class="text-center">
    <a href="https://rebellion.earth/the-truth/about-us/" target="_blank">XR International - About Us</a>
</div>

---
title: '03'
class: 'underline yellow-line'
---

# Citizens' Assembly{.red-line .inline-block #citizens-assembly}

We do not trust our government to make the bold, swift, and long-term actions necessary to achieve societal transformation. Instead, we demand a Citizens' Assembly to oversee the changes, creating a democracy fit for the purpose of averting climate catastrophe and empowering the voices and concerns of citizens over politicians. 

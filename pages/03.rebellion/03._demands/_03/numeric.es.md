---
title: '03'
class: 'underline yellow-line'
---

# ASEMBLEAS DE CIUDADANOS{.red-line .inline-block #citizens-assembly}

La creación de Asambleas de Ciudadanos Municipales y del Condado, legalmente facultadas para supervisar los cambios requeridos anteriormente, como parte de la creación de una democracia apta para detener el calentamiento global y empoderar a los ciudadanos sobre un establecimiento que nos ha brindado este peligroso clima. Estas Asambleas operarán desde una posición basada en la ciencia y la evidencia para garantizar que Los Ángeles pueda cumplir o superar su objetivo de
emisiones netas de carbono cero en todo el Condado para el año 2025.
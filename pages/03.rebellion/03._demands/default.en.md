---
title: Demands
classes: modular-demands-full
---

# Demands of XRLA{.red-line}

[plugin:page-inject](/rebellion/_demands/_01)
[plugin:page-inject](/rebellion/_demands/_02)
[plugin:page-inject](/rebellion/_demands/_03)
[plugin:page-inject](/rebellion/_demands/_04)

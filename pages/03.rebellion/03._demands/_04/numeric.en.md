---
title: '04'
---

# Just Transition{.red-line .inline-block #just-transition}

We demand a just transition away from fossil fuels that prioritizes indigenous sovereignty, reparations, and remediation, led by and for people of color and impacted communities to put an end to structural environmental injustice. We demand that ecosystems be granted the legal right to thrive and regenerate in perpetuity. Repairing the effects of historical and ongoing ecocide is the first step to preventing a catastrophic global extinction, and growing a world that is livable and just for all. 

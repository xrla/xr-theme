---
title: '04'
---

# UNA TRANSICIÓN JUSTA{.red-line .inline-block #just-transition}

XRLA requiere una transición justa que dé prioridad a las personas más vulnerables de Los Ángeles, la soberanía indígena, y establezca reparaciones y remediaciones, lideradas por y para, los afroamericanos, los pueblos indígenas, las personas de color y las comunidades pobres de Los Ángeles como recompensa por sus sufridos años de injusticia ambiental estructural. XRLA entiende que establecer derechos legales para los ecosistemas es un requisito no negociable del gobierno municipal y del condado para que los ecosistemas históricamente degradados prosperen y se regeneren a perpetuidad. La reparación de los efectos del ecocidio histórico y continuo en Los Ángeles es absolutamente obligatoria para prevenir la extinción de nuestra especie humana y de *todas* las de la Tierra.
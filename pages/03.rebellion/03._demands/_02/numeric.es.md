---
title: '02'
class: 'underline yellow-line'
---

# CERO NETOS PARA 2025{.red-line .inline-block #net-zero}

Que los gobiernos del Condado de Los Ángeles deben adoptar medidas de política legalmente vinculantes para reducir las emisiones de carbono a cero netos para 2025<sup><a href="#footnote3">2</a></sup> y para reducir los niveles de consumo e impacto en los gobiernos del Condado y los Gobiernos Municipales y entre todos los ciudadanos del Condado de Los Ángeles.
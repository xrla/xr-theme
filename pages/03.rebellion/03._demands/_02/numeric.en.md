---
title: '02'
class: 'underline yellow-line'
---

# Net Zero By 2025{.red-line .inline-block #net-zero}

California's governments must enact legally-binding policy measures to reduce carbon emissions to net zero by 2025, and to reduce consumption levels and ecological impacts across the state. They must cooperate internationally so that the global economy runs on no more than half a planet's worth of resources per year.

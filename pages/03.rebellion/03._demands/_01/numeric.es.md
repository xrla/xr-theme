---
title: '01'
class: 'underline yellow-line'
---

# DI LA VERDAD{.red-line .inline-block #tell-the-truth}

Que los gobiernos de Los Ángeles, desde el condado hasta el nivel municipal, deben decir la verdad sobre el calentamiento global y la emergencia ecológica más amplia que hemos creado sin la excepción del departamento. Estos gobiernos y sus departamentos deben revertir las políticas inconsistentes al abordar estos problemas y trabajar con los medios de comunicación para comunicar a los ciudadanos los peligros que enfrentamos y los cambios que deben realizarse para mitigar los impactos futuros del calentamiento global en la población y el entorno natural de Los Ángeles. Los gobiernos de Los Ángeles y sus departamentos no deben ser perpetuadores de demoras depredadoras.<sup><a href="#footnote3">1</a></sup>  
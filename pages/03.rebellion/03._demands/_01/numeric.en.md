---
title: '01'
class: 'underline yellow-line'
---

# Tell the truth{.red-line .inline-block #tell-the-truth}

We demand that our government must tell the truth about the climate and wider ecological emergency, and it must work to reverse all policies not in alignment with that position. We must work alongside the media to communicate the urgency for change in the face of imminent danger, including what individuals, communities, and businesses need to do.

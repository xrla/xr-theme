---
title: 'Metas'
classes: 'bg-lightgrey'
---

# Metas Fundamentales de XRLA{.red-line}

XRLA aprecia los principios del grupo internacional XR<sup><a href="#footnote2">1</a></sup> con un enfoque en Los Ángeles. En resumen, esto significa que XRLA es un grupo de activistas ecológicos que intenta crear un mundo apropiado para una vivienda saludable para las generaciones venideras, libre de los peligros existenciales de las consecuencias del calentamiento global. La misión de XRLA de concentrarse en lo que es necesario para lograr este objetivo se basa en el activismo no violento y la desobediencia civil al servicio de movilizar a una amplia coalición de *un mínimo* del 3.5% de la población de Los Ángeles (355,750 personas) lista para ser Disponible para acciones regulares y repetidas al servicio de lograr los objetivos de XRLA. XRLA es parte de un objetivo nacional de XR de aumentar el 3.5% o más de la población de los EE. UU. (11,497,200 personas) para luchar para cambiar la inhumanidad del clima actual y las normas que destruyen el medio ambiente.

<a name="footnote2"></a>
<ul class="footnotes">
<li><em><sup>1</sup> https://risingup.org.uk/about</em></li>
</ul>
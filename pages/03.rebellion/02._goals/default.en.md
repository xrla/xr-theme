---
title: 'Goals'
classes: 'bg-lightgrey'
---

# Our Mission{.red-line}

To spark and sustain a spirit of collective, creative rebellion which will enable much needed changes in our political, economic, and social landscape. We work to transform our society into one that is compassionate, inclusive, kind, sustainable, equitable, and interpersonally connected. 

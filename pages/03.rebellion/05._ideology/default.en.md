---
title: 'Ideology'
classes: 'bg-blue'
---

# Our Ideology{.red-line}

We intend to spark and sustain a spirit of collective, creative rebellion to motivate changes in our political, economic, and social landscape. We work with and beside other XR cells to focus consciousness on saving our ecosystems at the local, national, and international level. We believe in fostering cooperation with every citizen to quickly reach the goal of living sustainably within our natural environment *as a part of it, not apart from it*.

We affirm the scientific understanding that a 1.5&deg;C increase in the global temperature over preindustrial averages will be the cause of severe, adverse, and consequential impacts on life as humanity understands it today. An increase of 2&deg;C or greater will have catastrophic, existential consequences for humanity and all of the earth's flora and fauna. This is a dangerous future that we will, *without exception*, inhabit should we continue with “business as usual”. 

This message can inspire despair and grief. XRLA maintains a goal of acknowledging this grief and turning it into positive action in the service of the future of our earth and all of its inhabitants. We are all living through a moment of incredible consequence, and we *must* rise to stop our own extinction.

We assert that the current capitalist system breeds toxic divisions. We embrace an intersectional and revolutionary philosophy that condemns all forms of oppression and violence. We believe in respecting and elevating the voices of the original inhabitants of this land and frontline communities suffering most from the pollution of the fossil fuel state.

We view US police forces as the protectors of an indefensible status quo. They enforce unjust laws, and often stand directly in the way of our attempts to stop the worsening climate emergency. We have no faith in the institutions of US law enforcement because they have attacked communities of color and the working class, destroyed progressive social movements, and continue to do so.

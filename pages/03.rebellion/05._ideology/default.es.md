---
title: 'LA ideología'
classes: 'bg-blue'
---

# LA ideología DE XRLA{.red-line}

XRLA tiene la intención de provocar y sostener un espíritu de rebelión colectiva y creativa que permitirá cambios muy necesarios en nuestro panorama político, económico y social. XRLA trabaja con y al lado de las células en otras ciudades y estados de los EE. UU. para cambiar la conciencia nacional y convertirla en una basada en la conservación de nuestros ecosistemas. XRLA cree en fomentar la cooperación dentro de nuestra ciudadanía para alcanzar rápidamente la meta de vivir de manera sostenible dentro de nuestro entorno natural *como parte de él, no aparte de él*. Trabajamos para transformar a nuestra sociedad en una que sea compasiva, inclusiva, amable, sostenible, equitativa e interpersonalmente y conectada. Además, nos esforzamos por movilizar y capacitar a los organizadores locales en los municipios de Los Ángeles fuera de en la Ciudad de Los Ángeles para que trabajen con sus comunidades locales para desarrollar, utilizando el marco XR, las herramientas que necesitan para abordar los problemas profundamente arraigados de Los
Ángeles en relación con las amenazas creadas por el calentamiento global.  

XRLA se alía con el amplio consenso científicamente aceptado de que alcanzar temperaturas globales promedio mayores o iguales a un aumento de 1.5&deg;C sobre los promedios preindustriales será la causa de impactos severos, adversos y consecuentes en la vida como la humanidad lo entiende hoy<sup><a href="#footnote1">1</a></sup> y eso un aumento de 2&deg;C o mayor tendrá consecuencias catastróficas y quizás existenciales para la especie humana y para toda la flora y fauna de la Tierra.<sup><a href="#footnote1">2</a></sup> XRLA reconoce que esta es la realidad; no un mito, una broma o una toma de poder por parte de fuerzas infames, sino un futuro peligroso que vivimos hoy y aquellos que podrían seguirnos sin voluntad, excepción, habitar deberíamos continuar con el “negocio como de costumbre”.  

Mantener +1.5&deg;C es nuestra mejor esperanza y será difícil de lograr. La ventana para alcanzar este punto de referencia se hace más pequeña cada día. *Este objetivo requerirá el sacrificio de cada angeleno. Las consecuencias de fallar son reales, aterradoras y significarán el fin de la vida en la tierra como lo entendemos hoy*.<sup><a href="#footnote1">3</a></sup> Es un mensaje que inspira desesperación y dolor, y XRLA mantiene el objetivo de reconocer este dolor y se alía con organizaciones locales capaces de ayudar a los angelinos a reconciliar este dolor y convertirlo en una acción positiva al servicio del futuro de nuestra tierra y de todos sus aspectos. habitantes Para bien o para mal, todos estamos viviendo un momento de consecuencias como el que la humanidad nunca ha visto
antes. Debemos levantarnos para cumplirlo.

<a name="footnote1"></a>
<ul class="footnotes">
<li><em><sup>1</sup><a href="https://www.un.org/sustainabledevelopment/blog/2018/10/special-climate-report-1-5oc-is-possible-but-requires-unprecedented-and-urgent-action/">https://www.un.org/sustainabledevelopment/blog/2018/10/special-climate-report-1-5oc-is-possible-but-requires-unprecedented-and-urgent-action/</a></em></li>
<li><em><sup>2</sup><a href="https://www.wri.org/blog/2018/10/half-degree-and-world-apart-difference-climate-impacts-between-15-c-and-2-c-warming">https://www.wri.org/blog/2018/10/half-degree-and-world-apart-difference-climate-impacts-between-15-c-and-2-c-warming</a></em></li>
<li><em><sup>3</sup><a href="http://nymag.com/intelligencer/2017/07/climate-change-earth-too-hot-for-humans.html">http://nymag.com/intelligencer/2017/07/climate-change-earth-too-hot-for-humans.html</a></em></li>
</ul>
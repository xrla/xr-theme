---
title: 'The Truth'
---
# The Truth{.purple-line}

We are facing an unprecedented global emergency. Life on Earth is in crisis: scientists agree we have entered a period of abrupt climate breakdown, and we are in the midst of a mass extinction of our own making. 

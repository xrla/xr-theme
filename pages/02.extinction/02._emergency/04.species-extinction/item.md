---
title: 1,000x
subtitle: "Species extinction rates 1,000 times normal"
external_link: 
--- 

A “biological annihilation” of wildlife in recent decades means the Sixth Mass Extinction in Earth’s history is under way.

Looking at the UK for example, the 2016  State of Nature report found that they were “among the most nature-depleted countries in the world”.  One in five British mammals are at risk of being lost from the countryside with the populations of hedgehogs and water voles declining by almost 70% in just the past 20 years. Whilst another new report  by the British Trust for Ornithology found that more than a quarter of British bird species are threatened, including the Puffin, the Nightingale and Curlew. Across Europe the abundance of farmland birds has fallen by 55% in the just the past three decades!
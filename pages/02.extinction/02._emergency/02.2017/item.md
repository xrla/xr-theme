---
title: 2017
subtitle: "World Scientists’ Warning to Humanity: A Second Notice"
external_link: https://academic.oup.com/bioscience/article/67/12/1026/4605229
--- 


In 2017, humanity was given a [second notice](https://academic.oup.com/bioscience/article/67/12/1026/4605229).  Over 15,000 scientists signed a new and even more urgently worded letter which warned that “To prevent widespread misery and catastrophic biodiversity loss, humanity must practice a more environmentally sustainable alternative to business as usual. This prescription was well articulated by the world’s leading scientists 25 years ago, but in most respects, we have not heeded their warning. Soon it will be too late to shift course away from our failing trajectory, and time is running out. We must recognize, in our day-to-day lives and in our governing institutions, that Earth with all its life is our only home.”
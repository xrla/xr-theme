---
title: 76%
subtitle: "Decline in flying insect biomass"
external_link: 
--- 

Catastrophic reductions in global insect populations have profound consequences for ecological food chains and human crop pollination.

There is strong evidence that many insect populations are under serious threat and are declining in many places across the globe. Multiple pressures might include habitat loss, agro-chemical pollutants, invasive species and climate change.
---
title: 12.8%
subtitle: "Arctic sea ice is now declining at a rate of 12.8 percent per decade"
external_link: 
internal_link:  
--- 

Arctic sea ice is now declining at a rate of 12.8 percent per decade.

Summer Arctic sea ice is predicted to disappear almost completely by the middle of this century.

<blockquote>“We may lose the summer ice cover as early as 2030. This is in itself much earlier than projections from nearly all climate model simulations.” – <cite>Prof. Mark Serreze Director of the National Snow and Ice Data Centre</cite></blockquote>
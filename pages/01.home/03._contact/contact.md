---
title: "Take Action"
menu: "Take Action"
div_id: can-form-area-extinction-rebellion-los-angeles
class: p-2 text-center bg-green
button_text: Join Us!
call_to_action: Interested in doing more?
---

---
title: 'Demandas de la página de inicio'
hide_git_sync_repo_url: false
menu: Demands
class: small
demands:
    -
        header: 'Di la verdad'
        icon: 'fas fa-bullhorn'
        class: pink
        text: 'Nuestro gobierno debe decir la verdad acerca de cuán mortal es nuestra situación climática y debe revertir todas las políticas que no estén alineadas con las terribles realidades de nuestro clima actual que amenaza la vida y las realidades ecológicas.'
        url: 'rebellion#tell-the-truth'
    -
        header: 'Neto cero para el 2025'
        icon: 'fas fa-leaf'
        class: yellow
        text: 'Nuestro gobierno debe promulgar rápidamente políticas legalmente vinculantes para reducir las emisiones de carbono a cero neto para 2025.'
        url: 'rebellion#net-zero'
    -
        header: 'Asamblea de ciudadanos'
        icon: 'fas fa-fist-raised'
        class: blue
        text: 'Las asambleas de ciudadanos se deben crear y habilitar para garantizar que estas políticas sean promulgadas por el gobierno, ¡y que funcionen!'
        url: 'rebellion#citizens-assembly'
    -
        header: 'Transición justa'
        icon: 'fas fa-balance-scale'
        class: purple
        text: 'Una transición justa hacia una gobernanza sostenible que reconozca y repare la injusticia ambiental histórica y estructural, así como los derechos legales para todos los ecosistemas, son partes no negociables de los requisitos de XRLA.'
        url: 'rebellion#just-transition'
---

# Demandas{.black-line}

---
title: Directory Structure
taxonomy:
    category: docs
---

## Root Directories
For the purposes of developing on Grav you should restrict yourself to the `/user` directory and we will consider this to be synonomous to the root directory for development purposes. Nothing should be developed outside of this directory.

### /accounts
The `/accounts` directory stores user account information. It will store **yaml** files that spells out user account and their access in the system.

### /alpha.xrla.org
This is a specific development configuration meant that is served up only when the site is served up on the **alpha.xrla.org** domain. It includes the debugger, error displaying, and other development configuration.

### /blueprints
Administration panel form blueprints.

### /config
Overall system, theme, plugin, default configurations for the main site.

### /data
A temporary files directory that stores data structures from the grav CMS.

### /localhost
This is a specific development configuration meant that is served up only when the site is served up on the **localhost** domain. It includes the debugger, error displaying, and other development configuration.

### /pages 
This is the sites full page structure. This mainly includes the sites structure as directories and pages as markdown files. Sometimes single pages are broken down as multiple sub-directories that contains a single section.

### /plugins
This directory includes the plugins that the site uses to render the site.

### /sites
This directory includes any sub-sites that the site has that are served as sub-directories.

### /themes
This directory includes the themes configuration and template files.
---
title: Overview
taxonomy:
    category: docs
---

The XRLA website is built on the Grav flat file CMS system. Our goal is to create an easy to maintain website, easy to update by our content creators, easy to deploy for our devops, and easy to customize by our developers.

## Grav CMS
Grav is a Fast, Simple, and Flexible file-based Web-platform. It uses the Markdown language to render simple to write markdown text into HTML.  
You can read the full **documentation** on the Grav CMS [here](https://learn.getgrav.org/).

## Site Structure

### Overall Stucture
This is the top level site structure.  

![overall structure](site-structure-overview.png)  

* /home - **Home** page  
* /extinction - **The Extinction** page  
* /rebellion - **The Rebellion** page  
* /news - **News** listing page  
* /resources - **Resources** listing page  
* /modules - **Holds Modules like Footer & Sidebar**  

### Home page structure
The Home page 
![home page](site-structure-home.png)  

* 